import random

#Initialisation des variables à zéro
nbVictoire = 0
nbDefaite = 0


while True :
    #Choix et initialisation des variables de jeu
    limiteRandom = int(input("Choisir un nombre maximum pour les nombres aléatoires : "))
    coupMax = int(input("Choisir le nombre de coup maximum possible : "))

    nombreRobot = random.randint(1,limiteRandom)
    resultat = "defaite"

    #Boucle pour une partie
    for coup in range(coupMax):
        nombreJoueur = int(input("\nChoisir un nombre entre 1 et "+str(limiteRandom)+" : "))

        if nombreJoueur == nombreRobot:
            resultat = "victoire"
            break
        elif nombreJoueur < nombreRobot:
            print("\n=> Il faut un nombre plus grand !")
        else:
            print("\n=> Il faut un nombre plus petit !")
    
    #Gestion des résultats
    print("\nResultat :",resultat)
    print("=> La partie s'est jouée en :",coup+1,"tour(s).")

    if resultat == "victoire":
        nbVictoire += 1
    else:
        nbDefaite += 1

    #Gestion de nouvelles parties
    rejouer = str(input("\nVoulez-vous rejouer une partie ? (O)ui ou (N)on: "))

    if rejouer == "Oui" or rejouer == "oui" or rejouer == "o" or rejouer == "O":
        print("\nSuper, continuons!\n")
    else:
        print("\nD'accord, une autre fois :)\n")
        break

#Affichage résumé de la partie
print("Résumé de partie:\n=> Nombre de vitoire Joueur =",nbVictoire,
"\n=> Nombre de victoire Robot =",nbDefaite,
"\n=> Nombre de partie jouée =",nbDefaite+nbVictoire)
